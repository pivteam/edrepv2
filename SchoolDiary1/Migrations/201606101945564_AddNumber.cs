namespace SchoolDiary1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Grades", "Number", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Grades", "Number");
        }
    }
}
