// <auto-generated />
namespace SchoolDiary1.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddNumber : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddNumber));
        
        string IMigrationMetadata.Id
        {
            get { return "201606101945564_AddNumber"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
