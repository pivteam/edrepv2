namespace SchoolDiary1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressesId = c.Int(nullable: false, identity: true),
                        Street = c.String(),
                        HouseNumber = c.String(),
                        FlatNumber = c.String(),
                        PostalCode = c.String(),
                        Place = c.String(),
                        Province = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.AddressesId);
            
            CreateTable(
                "dbo.Guardians",
                c => new
                    {
                        GuardiansId = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        MiddleName = c.String(),
                        BirthDate = c.DateTime(),
                        PESEL = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        IdentityCardNumber = c.String(),
                        Address_AddressesId = c.Int(),
                    })
                .PrimaryKey(t => t.GuardiansId)
                .ForeignKey("dbo.Addresses", t => t.Address_AddressesId)
                .ForeignKey("dbo.LoginsPasswords", t => t.GuardiansId)
                .Index(t => t.GuardiansId)
                .Index(t => t.Address_AddressesId);
            
            CreateTable(
                "dbo.LoginsPasswords",
                c => new
                    {
                        LoginsPasswordsId = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.LoginsPasswordsId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentsId = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        MiddleName = c.String(),
                        BirthDate = c.DateTime(),
                        PESEL = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        SchoolIdentityCard = c.String(),
                        Address_AddressesId = c.Int(),
                        Class_ClassesId = c.Int(),
                    })
                .PrimaryKey(t => t.StudentsId)
                .ForeignKey("dbo.Addresses", t => t.Address_AddressesId)
                .ForeignKey("dbo.Classes", t => t.Class_ClassesId)
                .ForeignKey("dbo.LoginsPasswords", t => t.StudentsId)
                .Index(t => t.StudentsId)
                .Index(t => t.Address_AddressesId)
                .Index(t => t.Class_ClassesId);
            
            CreateTable(
                "dbo.Attendances",
                c => new
                    {
                        AttendancesId = c.Int(nullable: false, identity: true),
                        AttendanceType = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Contents = c.String(),
                        ClassSubject_ClassesSubjectsId = c.Int(),
                        Student_StudentsId = c.Int(),
                    })
                .PrimaryKey(t => t.AttendancesId)
                .ForeignKey("dbo.ClassesSubjects", t => t.ClassSubject_ClassesSubjectsId)
                .ForeignKey("dbo.Students", t => t.Student_StudentsId)
                .Index(t => t.ClassSubject_ClassesSubjectsId)
                .Index(t => t.Student_StudentsId);
            
            CreateTable(
                "dbo.ClassesSubjects",
                c => new
                    {
                        ClassesSubjectsId = c.Int(nullable: false, identity: true),
                        Class_ClassesId = c.Int(),
                        Subject_SubjectsId = c.Int(),
                        Teacher_TeachersId = c.Int(),
                    })
                .PrimaryKey(t => t.ClassesSubjectsId)
                .ForeignKey("dbo.Classes", t => t.Class_ClassesId)
                .ForeignKey("dbo.Subjects", t => t.Subject_SubjectsId)
                .ForeignKey("dbo.Teachers", t => t.Teacher_TeachersId)
                .Index(t => t.Class_ClassesId)
                .Index(t => t.Subject_SubjectsId)
                .Index(t => t.Teacher_TeachersId);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        ClassesId = c.Int(nullable: false, identity: true),
                        ClassName = c.String(),
                        Teacher_TeachersId = c.Int(),
                    })
                .PrimaryKey(t => t.ClassesId)
                .ForeignKey("dbo.Teachers", t => t.Teacher_TeachersId)
                .Index(t => t.Teacher_TeachersId);
            
            CreateTable(
                "dbo.NewsLetter",
                c => new
                    {
                        NewsLetterId = c.Int(nullable: false, identity: true),
                        Contents = c.String(),
                        Date = c.DateTime(nullable: false),
                        Class_ClassesId = c.Int(),
                        Teacher_TeachersId = c.Int(),
                    })
                .PrimaryKey(t => t.NewsLetterId)
                .ForeignKey("dbo.Classes", t => t.Class_ClassesId)
                .ForeignKey("dbo.Teachers", t => t.Teacher_TeachersId)
                .Index(t => t.Class_ClassesId)
                .Index(t => t.Teacher_TeachersId);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        TeachersId = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        MiddleName = c.String(),
                        BirthDate = c.DateTime(),
                        PESEL = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        Degree = c.Int(nullable: false),
                        IdentityCardNumber = c.String(),
                        Address_AddressesId = c.Int(),
                    })
                .PrimaryKey(t => t.TeachersId)
                .ForeignKey("dbo.Addresses", t => t.Address_AddressesId)
                .ForeignKey("dbo.LoginsPasswords", t => t.TeachersId)
                .Index(t => t.TeachersId)
                .Index(t => t.Address_AddressesId);
            
            CreateTable(
                "dbo.CardSubjects",
                c => new
                    {
                        CardSubjectsId = c.Int(nullable: false, identity: true),
                        FinalGrade = c.Int(nullable: false),
                        Student_StudentsId = c.Int(),
                        Subject_SubjectsId = c.Int(),
                        Teacher_TeachersId = c.Int(),
                    })
                .PrimaryKey(t => t.CardSubjectsId)
                .ForeignKey("dbo.Students", t => t.Student_StudentsId)
                .ForeignKey("dbo.Subjects", t => t.Subject_SubjectsId)
                .ForeignKey("dbo.Teachers", t => t.Teacher_TeachersId)
                .Index(t => t.Student_StudentsId)
                .Index(t => t.Subject_SubjectsId)
                .Index(t => t.Teacher_TeachersId);
            
            CreateTable(
                "dbo.Grades",
                c => new
                    {
                        GradesId = c.Int(nullable: false, identity: true),
                        Grade = c.Int(nullable: false),
                        GradeWeight = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Contents = c.String(),
                    })
                .PrimaryKey(t => t.GradesId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectsId = c.Int(nullable: false, identity: true),
                        SubjectName = c.String(),
                    })
                .PrimaryKey(t => t.SubjectsId);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        NotesId = c.Int(nullable: false, identity: true),
                        Contents = c.String(),
                        Date = c.DateTime(nullable: false),
                        Student_StudentsId = c.Int(),
                        Teacher_TeachersId = c.Int(),
                    })
                .PrimaryKey(t => t.NotesId)
                .ForeignKey("dbo.Students", t => t.Student_StudentsId)
                .ForeignKey("dbo.Teachers", t => t.Teacher_TeachersId)
                .Index(t => t.Student_StudentsId)
                .Index(t => t.Teacher_TeachersId);
            
            CreateTable(
                "dbo.GradesCardSubjects",
                c => new
                    {
                        Grades_GradesId = c.Int(nullable: false),
                        CardSubjects_CardSubjectsId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Grades_GradesId, t.CardSubjects_CardSubjectsId })
                .ForeignKey("dbo.Grades", t => t.Grades_GradesId, cascadeDelete: true)
                .ForeignKey("dbo.CardSubjects", t => t.CardSubjects_CardSubjectsId, cascadeDelete: true)
                .Index(t => t.Grades_GradesId)
                .Index(t => t.CardSubjects_CardSubjectsId);
            
            CreateTable(
                "dbo.StudentsGuardians",
                c => new
                    {
                        Students_StudentsId = c.Int(nullable: false),
                        Guardians_GuardiansId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Students_StudentsId, t.Guardians_GuardiansId })
                .ForeignKey("dbo.Students", t => t.Students_StudentsId, cascadeDelete: true)
                .ForeignKey("dbo.Guardians", t => t.Guardians_GuardiansId, cascadeDelete: true)
                .Index(t => t.Students_StudentsId)
                .Index(t => t.Guardians_GuardiansId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Guardians", "GuardiansId", "dbo.LoginsPasswords");
            DropForeignKey("dbo.Students", "StudentsId", "dbo.LoginsPasswords");
            DropForeignKey("dbo.StudentsGuardians", "Guardians_GuardiansId", "dbo.Guardians");
            DropForeignKey("dbo.StudentsGuardians", "Students_StudentsId", "dbo.Students");
            DropForeignKey("dbo.Attendances", "Student_StudentsId", "dbo.Students");
            DropForeignKey("dbo.Students", "Class_ClassesId", "dbo.Classes");
            DropForeignKey("dbo.Notes", "Teacher_TeachersId", "dbo.Teachers");
            DropForeignKey("dbo.Notes", "Student_StudentsId", "dbo.Students");
            DropForeignKey("dbo.NewsLetter", "Teacher_TeachersId", "dbo.Teachers");
            DropForeignKey("dbo.Teachers", "TeachersId", "dbo.LoginsPasswords");
            DropForeignKey("dbo.ClassesSubjects", "Teacher_TeachersId", "dbo.Teachers");
            DropForeignKey("dbo.Classes", "Teacher_TeachersId", "dbo.Teachers");
            DropForeignKey("dbo.CardSubjects", "Teacher_TeachersId", "dbo.Teachers");
            DropForeignKey("dbo.ClassesSubjects", "Subject_SubjectsId", "dbo.Subjects");
            DropForeignKey("dbo.CardSubjects", "Subject_SubjectsId", "dbo.Subjects");
            DropForeignKey("dbo.CardSubjects", "Student_StudentsId", "dbo.Students");
            DropForeignKey("dbo.GradesCardSubjects", "CardSubjects_CardSubjectsId", "dbo.CardSubjects");
            DropForeignKey("dbo.GradesCardSubjects", "Grades_GradesId", "dbo.Grades");
            DropForeignKey("dbo.Teachers", "Address_AddressesId", "dbo.Addresses");
            DropForeignKey("dbo.NewsLetter", "Class_ClassesId", "dbo.Classes");
            DropForeignKey("dbo.ClassesSubjects", "Class_ClassesId", "dbo.Classes");
            DropForeignKey("dbo.Attendances", "ClassSubject_ClassesSubjectsId", "dbo.ClassesSubjects");
            DropForeignKey("dbo.Students", "Address_AddressesId", "dbo.Addresses");
            DropForeignKey("dbo.Guardians", "Address_AddressesId", "dbo.Addresses");
            DropIndex("dbo.StudentsGuardians", new[] { "Guardians_GuardiansId" });
            DropIndex("dbo.StudentsGuardians", new[] { "Students_StudentsId" });
            DropIndex("dbo.GradesCardSubjects", new[] { "CardSubjects_CardSubjectsId" });
            DropIndex("dbo.GradesCardSubjects", new[] { "Grades_GradesId" });
            DropIndex("dbo.Notes", new[] { "Teacher_TeachersId" });
            DropIndex("dbo.Notes", new[] { "Student_StudentsId" });
            DropIndex("dbo.CardSubjects", new[] { "Teacher_TeachersId" });
            DropIndex("dbo.CardSubjects", new[] { "Subject_SubjectsId" });
            DropIndex("dbo.CardSubjects", new[] { "Student_StudentsId" });
            DropIndex("dbo.Teachers", new[] { "Address_AddressesId" });
            DropIndex("dbo.Teachers", new[] { "TeachersId" });
            DropIndex("dbo.NewsLetter", new[] { "Teacher_TeachersId" });
            DropIndex("dbo.NewsLetter", new[] { "Class_ClassesId" });
            DropIndex("dbo.Classes", new[] { "Teacher_TeachersId" });
            DropIndex("dbo.ClassesSubjects", new[] { "Teacher_TeachersId" });
            DropIndex("dbo.ClassesSubjects", new[] { "Subject_SubjectsId" });
            DropIndex("dbo.ClassesSubjects", new[] { "Class_ClassesId" });
            DropIndex("dbo.Attendances", new[] { "Student_StudentsId" });
            DropIndex("dbo.Attendances", new[] { "ClassSubject_ClassesSubjectsId" });
            DropIndex("dbo.Students", new[] { "Class_ClassesId" });
            DropIndex("dbo.Students", new[] { "Address_AddressesId" });
            DropIndex("dbo.Students", new[] { "StudentsId" });
            DropIndex("dbo.Guardians", new[] { "Address_AddressesId" });
            DropIndex("dbo.Guardians", new[] { "GuardiansId" });
            DropTable("dbo.StudentsGuardians");
            DropTable("dbo.GradesCardSubjects");
            DropTable("dbo.Notes");
            DropTable("dbo.Subjects");
            DropTable("dbo.Grades");
            DropTable("dbo.CardSubjects");
            DropTable("dbo.Teachers");
            DropTable("dbo.NewsLetter");
            DropTable("dbo.Classes");
            DropTable("dbo.ClassesSubjects");
            DropTable("dbo.Attendances");
            DropTable("dbo.Students");
            DropTable("dbo.LoginsPasswords");
            DropTable("dbo.Guardians");
            DropTable("dbo.Addresses");
        }
    }
}
