﻿using SchoolDiary1.Design;
using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace SchoolDiary1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        object Teacher;
        object Guardian;
        object Student;

        private LoginWindow logWindow = Application.Current.Windows[0] as LoginWindow;

        public MainWindow()
        {
            InitializeComponent();
            //PeselTempLabel.Content = logWindow.LoginTextBox.Text;
            //logWindow.Close();
            profileMenuItem.Visibility = Visibility.Collapsed;
            diaryMenuItem.Visibility = Visibility.Collapsed;
            NameTextBox.Text = "admin";
            teacherCanvas.Visibility = Visibility.Hidden;
            StudentGuardianStackPanel.Visibility = Visibility.Hidden;
            adminComboBox.Visibility = Visibility.Visible;
        }

        public MainWindow(object o)
        {
            InitializeComponent();
            PeselTempLabel.Content = logWindow.LoginTextBox.Text;

            if (o is Teachers)
            {
                Teacher = o as Teachers;
                ShowClassAndSubjectsOnComboBox();
                NameTextBox.Text = ShowName();
                personalizationMenuItem.Visibility = Visibility.Collapsed;
                //diaryMenuItem.Visibility = Visibility.Collapsed;
                StudentGuardianStackPanel.Visibility = Visibility.Hidden;
                teacherCanvas.Visibility = Visibility.Visible;

            }

            else if (o is Guardians)
            {
                Guardian = o as Guardians;
                NameTextBox.Text = ShowName();
                StudentGuardianStackPanel.Visibility = Visibility.Visible;
                teacherCanvas.Visibility = Visibility.Hidden;
                //personalizationMenuItem.Visibility = Visibility.Hidden;
                personalizationMenuItem.Visibility = Visibility.Collapsed;
                diaryMenuItem.Visibility = Visibility.Collapsed;
            }

            else if (o is Students)
            {
                Student = o as Students;
                NameTextBox.Text = ShowName();
                StudentGuardianStackPanel.Visibility = Visibility.Visible;
                teacherCanvas.Visibility = Visibility.Hidden;
                //personalizationMenuItem.Visibility = Visibility.Hidden;
                personalizationMenuItem.Visibility = Visibility.Collapsed;
                diaryMenuItem.Visibility = Visibility.Collapsed;
            }


            logWindow.Close();
        }

        private void ShowStudentsOnDataGrid()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var students = from st in context.Students
                               from clas in context.Classes
                               from classub in clas.ClassesSubjects
                               from sub in context.Subjects
                               from cardsub in sub.CardSubjects
                               from grade in cardsub.Grades
                               where clas.ClassName == classComboBox.SelectedItem.ToString()
                               where sub.SubjectName == subjectComboBox.SelectedItem.ToString()
                               select new
                               {
                                   Imie = st.FirstName,
                                   Nazwisko = st.LastName,
                                   Ocena = grade.Grade,
                                   Waga = grade.GradeWeight,
                                   Data = grade.GradeWeight
                               };

                dataGrid.ItemsSource = students.ToList();
            }
        }

        private void ShowClassAndSubjectsOnComboBox()
        {
            classComboBox.Items.Refresh();
            subjectComboBox.Items.Refresh();
            List<string> classesList = new List<string>();
            List<string> subjectsList = new List<string>();

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var classes = (from t in context.Teachers
                               from clsub in t.ClassesSubjects
                               from cl in context.Classes
                               from sub in context.Subjects
                               where cl.ClassesId == clsub.Class.ClassesId
                               where t.PESEL == logWindow.LoginTextBox.Text
                               select cl).ToList();

                var subjects = (from t in context.Teachers
                                from clsub in t.ClassesSubjects
                                from cl in context.Classes
                                from sub in context.Subjects
                                where sub.SubjectsId == clsub.Subject.SubjectsId
                                where t.PESEL == logWindow.LoginTextBox.Text
                                select sub).ToList();



                foreach (var item in classes.Distinct())
                    classesList.Add(item.ClassName);

                foreach (var item in subjects.Distinct())
                    subjectsList.Add(item.SubjectName);

                classComboBox.ItemsSource = classesList;
                subjectComboBox.ItemsSource = subjectsList;
            }
        }

        private string ShowName()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                if (Teacher is Teachers)
                {
                    var teacherName = context.Teachers
                        .Where(x => x.PESEL == logWindow.LoginTextBox.Text).FirstOrDefault();
                    return teacherName.FirstName + " " + teacherName.LastName + " " + teacherName.PESEL;
                }
                else if (Guardian is Guardians)
                {
                    var guardianName = context.Guardians
                        .Where(x => x.PESEL == logWindow.LoginTextBox.Text).FirstOrDefault();
                    return guardianName.FirstName + " " + guardianName.LastName + " " + guardianName.PESEL;
                }
                else if (Student is Students)
                {
                    var studentName = context.Students
                        .Where(x => x.PESEL == logWindow.LoginTextBox.Text).FirstOrDefault();
                    return studentName.FirstName + " " + studentName.LastName + " " + studentName.PESEL;
                }
                else
                    return null;
            }
        }

        private void ShowDataOnStudents()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                if (WhatComboBox.SelectedIndex == 0)
                {
                    if (studentSubjectComboBox.SelectedIndex == 0)
                    {
                        var query = from student in context.Students
                                    from cardSubject in student.CardSubjects
                                    from subject in context.Subjects
                                        //from cardSub in subject.CardSubjects
                                    from grade in cardSubject.Grades
                                    where student.PESEL == logWindow.LoginTextBox.Text
                                    where student.StudentsId == cardSubject.Student.StudentsId
                                    where subject.SubjectsId == cardSubject.Subject.SubjectsId
                                    //where cardSubject.Subject.SubjectName == studentSubjectComboBox.SelectedItem.ToString()
                                    //where cardSubject.Grades.Any(c => c.GradesId == cardSubject.CardSubjectsId)
                                    select new
                                    {
                                        NazwaPrzedmiotu = subject.SubjectName,
                                        Ocena = grade.Grade,
                                        Waga = grade.GradeWeight,
                                        Data = grade.Date,
                                        Opis = grade.Contents
                                    };

                        dataGrid.ItemsSource = query.ToList().Distinct();
                    }
                    else
                    {
                        var query = from student in context.Students
                                    from cardSubject in student.CardSubjects
                                    from subject in context.Subjects
                                        //from cardSub in subject.CardSubjects
                                    from grade in cardSubject.Grades
                                    where student.PESEL == logWindow.LoginTextBox.Text
                                    where student.StudentsId == cardSubject.Student.StudentsId
                                    where subject.SubjectsId == cardSubject.Subject.SubjectsId
                                    where cardSubject.Subject.SubjectName == studentSubjectComboBox.SelectedItem.ToString()
                                    //where cardSubject.Grades.Any(c => c.GradesId == cardSubject.CardSubjectsId)
                                    select new
                                    {
                                        NazwaPrzedmiotu = subject.SubjectName,
                                        Ocena = grade.Grade,
                                        Waga = grade.GradeWeight,
                                        Data = grade.Date,
                                        Opis = grade.Contents
                                    };

                        dataGrid.ItemsSource = query.ToList().Distinct();
                    }
                }
                else if (WhatComboBox.SelectedIndex == 1)
                {
                    var query1 = from student in context.Students
                                 from cardSubject in student.CardSubjects
                                 from subject in context.Subjects
                                 where student.PESEL == logWindow.LoginTextBox.Text
                                 where student.StudentsId == cardSubject.Student.StudentsId
                                 where subject.SubjectsId == cardSubject.Subject.SubjectsId
                                 select new
                                 {
                                     NazwaPrzedmiotu = subject.SubjectName,
                                     OcenaKońcowa = cardSubject.FinalGrade
                                 };

                    dataGrid.ItemsSource = query1.ToList().Distinct();
                }
            }
        }

        private void ShowDataOnGuardians()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                if (WhatComboBox.SelectedIndex == 0)
                {
                    if (studentSubjectComboBox.SelectedIndex != 0)
                    {
                        var query = from student in context.Students
                                    from guard in student.Guardians
                                    from cardSubject in student.CardSubjects
                                    from subject in context.Subjects
                                    from grade in cardSubject.Grades
                                    where guard.PESEL == logWindow.LoginTextBox.Text
                                    where student.StudentsId == cardSubject.Student.StudentsId
                                    where subject.SubjectsId == cardSubject.Subject.SubjectsId
                                    select new
                                    {
                                        NazwaPrzedmiotu = subject.SubjectName,
                                        Ocena = grade.Grade,
                                        Waga = grade.GradeWeight,
                                        Data = grade.Date,
                                        Opis = grade.Contents
                                    };

                        dataGrid.ItemsSource = query.ToList().Distinct();
                    }
                    else
                    {
                        var query = from student in context.Students
                                    from guard in student.Guardians
                                    from cardSubject in student.CardSubjects
                                    from subject in context.Subjects
                                    from grade in cardSubject.Grades
                                    where guard.PESEL == logWindow.LoginTextBox.Text
                                    where student.StudentsId == cardSubject.Student.StudentsId
                                    where subject.SubjectsId == cardSubject.Subject.SubjectsId
                                    where cardSubject.Subject.SubjectName == studentSubjectComboBox.SelectedItem.ToString()
                                    select new
                                    {
                                        NazwaPrzedmiotu = subject.SubjectName,
                                        Ocena = grade.Grade,
                                        Waga = grade.GradeWeight,
                                        Data = grade.Date,
                                        Opis = grade.Contents
                                    };

                        dataGrid.ItemsSource = query.ToList().Distinct();
                    }
                }
                else if (WhatComboBox.SelectedIndex == 1)
                {
                    var query1 = from student in context.Students
                                 from guard in student.Guardians
                                 from cardSubject in student.CardSubjects
                                 from subject in context.Subjects
                                 where guard.PESEL == logWindow.LoginTextBox.Text
                                 where student.StudentsId == cardSubject.Student.StudentsId
                                 where subject.SubjectsId == cardSubject.Subject.SubjectsId
                                 select new
                                 {
                                     NazwaPrzedmiotu = subject.SubjectName,
                                     OcenaKońcowa = cardSubject.FinalGrade
                                 };

                    dataGrid.ItemsSource = query1.ToList().Distinct();
                }
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                if (Guardian is Guardians)
                {
                    //MessageBox.Show("Opiekun");
                    ShowDataOnGuardians();
                }
                else if (Student is Students)
                {
                    //MessageBox.Show("Uczeń");
                    ShowDataOnStudents();
                }
                else
                {
                    var teachers = context.Teachers
                        .Select(x => new
                        {
                            Imie = x.FirstName,
                            Nazwisko = x.LastName,
                            DrugieImie = x.MiddleName,
                            DataUrodzenia = x.BirthDate,
                            Pesel = x.PESEL,
                            NumerTelefonu = x.PhoneNumber,
                            Email = x.Email,
                            Tytuł = x.Degree.ToString(),
                            NumerDowodu = x.IdentityCardNumber
                        });
                    dataGrid.ItemsSource = teachers.ToList();
                }
            }

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            TeacherWindow teacherWindow = new TeacherWindow();
            //teacherWindow.Show();
            teacherWindow.ShowDialog();
            //this.IsEnabled = false;
            //logWindow.Close();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            GuardianWindow guardianWindow = new GuardianWindow();
            //guardianWindow.Show();
            guardianWindow.ShowDialog();
            //this.IsEnabled = false;
            //logWindow.Close();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            StudentWindow studentWindow = new StudentWindow();
            //studentWindow.Show();
            studentWindow.ShowDialog();
            //this.IsEnabled = false;
            //logWindow.Close();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            ClassWindows classWindow = new ClassWindows();
            //classWindow.Show();
            classWindow.ShowDialog();
            //this.IsEnabled = false;
            //logWindow.Close();
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            SubjectWindow subWindow = new SubjectWindow();
            // subWindow.Show();
            subWindow.ShowDialog();
            //this.IsEnabled = false;
            //logWindow.Close();
        }
        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            EditProfileWindow editProfWindow = new EditProfileWindow();
            //editProfWindow.Show();
            editProfWindow.ShowDialog();
            //this.IsEnabled = false;
            //logWindow.Close();
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            RateWindow rateWin = new RateWindow();
            //rateWin.Show();
            rateWin.ShowDialog();
            //this.IsEnabled = false;
            //logWindow.Close();
        }

        private void LogOutTextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            LoginWindow logWin = new LoginWindow();
            logWin.Show();
            this.Close();
        }

        private void confirmClassButton_Click(object sender, RoutedEventArgs e)
        {
            if (Teacher is Teachers)
            {
                //MessageBox.Show("nauczyciel");
                ShowStudentsOnDataGrid();

            }
        }

        private void MenuItem_Click_7(object sender, RoutedEventArgs e)
        {

            //MessageBox.Show("Czy na pewno chcesz usunąć konto?", "Usunięcie konta", MessageBoxButton.OKCancel);
            if (MessageBox.Show("Czy na pewno chcesz usunąć konto?", "Usunięcie konta", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                using (SchoolDiaryContext context = new SchoolDiaryContext())
                {
                    if (Teacher is Teachers)
                    {
                        var teacher = context.Teachers
                            .Where(x => x.PESEL == logWindow.LoginTextBox.Text).FirstOrDefault();

                        teacher.LoginPassword.Password = null;
                    }
                    else if (Guardian is Guardians)
                    {
                        var guardian = context.Guardians
                            .Where(x => x.PESEL == logWindow.LoginTextBox.Text).FirstOrDefault();

                        guardian.LoginPassword.Password = null;
                    }
                    else if (Student is Students)
                    {
                        var student = context.Students
                            .Where(x => x.PESEL == logWindow.LoginTextBox.Text).FirstOrDefault();

                        student.LoginPassword.Password = null;
                    }
                    context.SaveChanges();
                }

                LoginWindow loginWindow = new LoginWindow();
                loginWindow.Show();
                this.Close();

            }
        }

        private void MenuItem_Click_8(object sender, RoutedEventArgs e)
        {
            AttendancesWindow attWin = new AttendancesWindow();
            attWin.ShowDialog();
        }

        private void WhatComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            studentSubjectComboBox.SelectedIndex = -1;
            studentSubjectComboBox.Items.Clear();
            studentSubjectComboBox.Items.Add("Wszystkie");

            if (WhatComboBox.SelectedIndex == 0)
            {
                confirmButton.IsEnabled = false;
                studentSubjectComboBox.IsEnabled = true;
                using (SchoolDiaryContext context = new SchoolDiaryContext())
                {
                    List<string> subjectList = new List<string>();

                    if (Guardian is Guardians)
                    {
                        var guard = context.Guardians.Where(x => x.PESEL == logWindow.LoginTextBox.Text).FirstOrDefault();
                        if (guard.Students.Count > 1)
                        {
                            guardianStudentsComboBox.Visibility = Visibility.Visible;
                            guardianStudentsComboBox.IsEnabled = true;
                            foreach (var item in guard.Students)
                            {
                                guardianStudentsComboBox.Items.Add(item.PESEL);
                            }

                        }
                        else
                        {
                            //confirmButton.IsEnabled = true;
                            var card = context.CardSubjects.Where(x => x.Student.Guardians.Any(y => y.PESEL == logWindow.LoginTextBox.Text));

                            foreach (var item in card)
                            {
                                studentSubjectComboBox.Items.Add(item.Subject.SubjectName);
                            }
                        }

                    }

                    else
                    {
                        //confirmButton.IsEnabled = true;
                        var card = context.CardSubjects.Where(x => x.Student.PESEL == logWindow.LoginTextBox.Text);

                        foreach (var item in card)
                        {
                            studentSubjectComboBox.Items.Add(item.Subject.SubjectName);
                        }
                    }

                }
            }
            else
            {
                studentSubjectComboBox.IsEnabled = false;
                confirmButton.IsEnabled = true;
            }
        }

        private void guardianStudentsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            confirmButton.IsEnabled = true;
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var card = context.CardSubjects.Where(x => x.Student.PESEL == guardianStudentsComboBox.SelectedItem.ToString());

                foreach (var item in card)
                {
                    studentSubjectComboBox.Items.Add(item.Subject.SubjectName);
                }
            }
        }

        private void studentSubjectComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            confirmButton.IsEnabled = true;
        }

        private void adminComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                if (adminComboBox.SelectedIndex==0)
                {
                    var query = context.Teachers;
                    dataGrid.ItemsSource = query.ToList();
                }
                else if (adminComboBox.SelectedIndex == 1)
                {
                    var query = context.Students;
                    dataGrid.ItemsSource = query.ToList();
                }
                else if (adminComboBox.SelectedIndex == 2)
                {
                    var query = context.Guardians;
                    dataGrid.ItemsSource = query.ToList();
                }
                else if (adminComboBox.SelectedIndex == 3)
                {
                    var query = context.Classes;
                    dataGrid.ItemsSource = query.ToList();
                }
                else if (adminComboBox.SelectedIndex == 4)
                {
                    var query = context.Subjects;
                    dataGrid.ItemsSource = query.ToList();
                }
            }
        }
    }
}
