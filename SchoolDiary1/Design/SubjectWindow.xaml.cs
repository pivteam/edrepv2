﻿using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolDiary1.Design
{
    /// <summary>
    /// Interaction logic for Subject.xaml
    /// </summary>
    public partial class SubjectWindow : Window
    {
        bool buttonAddWasClicked = false;
        bool buttonDeleteWasClicked = false;
        bool buttonEditWasClicked = false;
        Dictionary<string, string> teacherDic = new Dictionary<string, string>();
        public SubjectWindow()
        {
            InitializeComponent();
            searchStackPanel.Visibility = Visibility.Hidden;
            saveButton.IsEnabled = clearButton.IsEnabled = StackPanel2.IsEnabled = false;

            personComboBox2.SelectedIndex = -1;
            subjectsComboBox.SelectedIndex = -1;
            ClassComboBox.SelectedIndex = -1;
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
               // List<string> teacherList = new List<string>();
                var teacher = context.Teachers.Where(x => x.PESEL != null).ToList();

                foreach (var item in teacher)
                    teacherDic.Add(item.PESEL,item.LastName+" "+item.FirstName);

                personComboBox2.ItemsSource = teacherDic.Values;
            }
            personComboBox2.IsEnabled = true;

            List<string> classesList = new List<string>();

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var classes = context.Classes
                    .Where(x => x.ClassName != null).ToList()
                    .OrderBy(x => x.ClassName);

                foreach (var item in classes)
                    classesList.Add(item.ClassName);

                ClassComboBox.ItemsSource = classesList;
            }

            subjectsComboBox.Items.Refresh();
            List<string> subjectsList = new List<string>();

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjects = context.Subjects
                    .Where(x => x.SubjectName != null).ToList()
                    .OrderBy(x => x.SubjectName);

                foreach (var item in subjects)
                    subjectsList.Add(item.SubjectName);

                subjectsComboBox.ItemsSource = subjectsList;
            }
        }


        private void Enable()
        {
            saveButton.IsEnabled = clearButton.IsEnabled = StackPanel2.IsEnabled = true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            buttonAddWasClicked = true;
            Enable();
            stackPanel1.IsEnabled = false;
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            buttonDeleteWasClicked = true;
            Enable();
            stackPanel1.IsEnabled = false;
        }
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            buttonEditWasClicked = true;
            searchStackPanel.Visibility = Visibility.Visible;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            if (buttonAddWasClicked == true)
                AddSubject();
            else if (buttonDeleteWasClicked == true)
                DeleteSubject();
            else if (buttonEditWasClicked == true)
                EditSubject();
        }

        private void AddSubject()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                Subjects subject = new Subjects()
                {
                    SubjectName = subjectTextBox.Text
                };

                var sub = context.Subjects.Where(x => x.SubjectName == subjectTextBox.Text).FirstOrDefault();

                if (sub != null)
                {
                    MessageBox.Show("Isnieje już przedmiot o takiej nazwie!");
                    subjectTextBox.Focus();
                }
                else
                {
                    context.Subjects.Add(subject);
                    context.SaveChanges();

                    MessageBox.Show("Przedmiot dodany!");

                    buttonAddWasClicked = false;
                    stackPanel1.IsEnabled = true;
                    StackPanel2.IsEnabled = false;
                    subjectTextBox.Text = null;
                    searchTextBox.Text = null;
                    saveButton.IsEnabled = false;
                    clearButton.IsEnabled = false;
                }
                subjectsComboBox.Items.Refresh();
            }

        }
        private void DeleteSubject()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjectToDelete = context.Subjects.Where(x => x.SubjectName == subjectTextBox.Text).FirstOrDefault();

                if (subjectToDelete != null)
                {
                    context.Subjects.Remove(subjectToDelete);
                    context.SaveChanges();

                    MessageBox.Show("Przedmiot usunięty!");

                    buttonDeleteWasClicked = false;
                    subjectTextBox.Text = null;
                    stackPanel1.IsEnabled = true;
                    StackPanel2.IsEnabled = false;
                    saveButton.IsEnabled = false;
                    clearButton.IsEnabled = false;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono przedmiotu o takiej nazwie!");
                    subjectTextBox.Focus();
                }
            }
            subjectsComboBox.Items.Refresh();
        }
        private void EditSubject()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjectToEdit = context.Subjects.Where(x => x.SubjectName == searchTextBox.Text).SingleOrDefault();

                subjectToEdit.SubjectName = subjectTextBox.Text;

                context.SaveChanges();

                buttonEditWasClicked = false;
                subjectTextBox.Text = null;
                stackPanel1.IsEnabled = true;
                saveButton.IsEnabled = false;
                clearButton.IsEnabled = false;
                StackPanel2.IsEnabled = false;

                MessageBox.Show("Zaktualizowano!");
            }
            subjectsComboBox.Items.Refresh();
        }

        private void searchTextBox_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            searchTextBox.Clear();
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjectToEdit = context.Subjects.Where(x => x.SubjectName == searchTextBox.Text).SingleOrDefault();

                if (subjectToEdit != null)
                {
                    searchStackPanel.Visibility = Visibility.Hidden;
                    StackPanel2.IsEnabled = true;

                    saveButton.IsEnabled = true;
                    clearButton.IsEnabled = true;

                    subjectTextBox.Text = subjectToEdit.SubjectName;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono takiego przedmiotu!");
                    searchTextBox.Focus();
                }
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            subjectTextBox.Text = searchTextBox.Text = null;
            stackPanel1.IsEnabled = true;
            saveButton.IsEnabled = clearButton.IsEnabled = false;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void ClearButton_Click_1(object sender, RoutedEventArgs e)
        {
            personComboBox2.SelectedIndex = subjectsComboBox.SelectedIndex = -1;
            personComboBox2.IsEnabled = subjectsComboBox.IsEnabled = false;
        }

        private void personComboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            subjectsComboBox.IsEnabled = true;
        }

        private void classComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AssignButton.IsEnabled = NoAssignButton.IsEnabled = true;
        }

        private void AssignButton_Click(object sender, RoutedEventArgs e)
        {

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {

                var Class = context.Classes.Where(x => x.ClassName == ClassComboBox.SelectedItem.ToString()).FirstOrDefault();
                string tmp=teacherDic.FirstOrDefault(x => x.Value.Contains(personComboBox2.SelectedItem.ToString())).Key;
                var Teacher = context.Teachers.Where(x => x.PESEL == tmp).FirstOrDefault();
                var Subject = context.Subjects.Where(x => x.SubjectName == subjectsComboBox.SelectedItem.ToString()).FirstOrDefault();

                var clasSubCOn = context.ClassesSubjects.ToList();
                bool ok = true;
                foreach (var item in clasSubCOn)
                    if (item.Class == Class && item.Teacher == Teacher && item.Subject == Subject)
                    {
                        ok = false;
                        MessageBox.Show("Taka relacja już istnieje!");
                        break;
                    }
                    else if (item.Class == Class && item.Subject == Subject)
                    {
                        ok = false;
                        item.Teacher = Teacher;
                        var students = context.Students.Where(x => x.PESEL != null && x.Class.ClassesId == Class.ClassesId).ToList();

                        foreach (var student in students)
                        {
                            var card = context.CardSubjects.Where(x => x.Student.StudentsId == student.StudentsId).ToList();
                            foreach (var cardItem in card)
                            {
                                if (cardItem.Subject == Subject)
                                    cardItem.Teacher = Teacher;
                            }
                        }
                        context.SaveChanges();
                        MessageBox.Show("Nauczyciel został zmieniony!");
                        break;
                    }
                if (ok == true)
                {
                    var students = context.Students.Where(x => x.Class.ClassesId == Class.ClassesId).ToList();
                    foreach (var student in students)
                    {
                        var findCard = context.CardSubjects.ToList();
                        bool tmpOK = true;
                        foreach (var item in findCard)
                        {
                            if (item.Student.StudentsId==student.StudentsId && item.Subject.SubjectName == Subject.SubjectName)
                            {
                                item.Teacher = Teacher;
                                item.Subject = Subject;
                                tmpOK = false;
                                break;
                            }
                        }
                        if (tmpOK == true)
                        {
                            CardSubjects card = new CardSubjects()
                            {
                                Student = student,
                                Teacher = Teacher,
                                Subject = Subject


                            };
                            //for (int i = 0; i < 10; i++)
                            //{
                            //    Grades grade = new Grades() { Number = i + 1 };
                            //    context.Grades.Add(grade);
                            //    card.Grades.Add(grade);
                            //}

                            context.CardSubjects.Add(card);
                        }
                    }
                    ClassesSubjects clasSub = new ClassesSubjects();
                    clasSub.Class = Class;
                    clasSub.Teacher = Teacher;
                    clasSub.Subject = Subject;
                    MessageBox.Show("Relacja została pomyślnie zapisana!");
                    context.ClassesSubjects.Add(clasSub);
                    context.SaveChanges();
                }

            }
        }

        private void NoAssignButton_Click(object sender, RoutedEventArgs e)
        {

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var Class = context.Classes.Where(x => x.ClassName == ClassComboBox.SelectedItem.ToString()).FirstOrDefault();
                string tmp = teacherDic.FirstOrDefault(x => x.Value.Contains(personComboBox2.SelectedItem.ToString())).Key;
                var Teacher = context.Teachers.Where(x => x.PESEL == tmp).FirstOrDefault();
                var Subject = context.Subjects.Where(x => x.SubjectName == subjectsComboBox.SelectedItem.ToString()).FirstOrDefault();

                var clasSubCOn = context.ClassesSubjects.ToList();
                bool ok = true;
                foreach (var item in clasSubCOn)
                    if (item.Class == Class && item.Teacher == Teacher && item.Subject == Subject)
                    {
                        context.ClassesSubjects.Remove(item);
                        context.SaveChanges();
                        MessageBox.Show("Relacja usunięta!");
                        ok = false;
                        break;
                    }
                if (ok == true)
                    MessageBox.Show("Taka relacja nie istnieje!");
                context.SaveChanges();
            }
        }
    }
}
