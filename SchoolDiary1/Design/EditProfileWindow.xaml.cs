﻿using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolDiary1.Design
{
    /// <summary>
    /// Interaction logic for EditProfileWindow.xaml
    /// </summary>
    public partial class EditProfileWindow : Window
    {
        //private LoginWindow logWindow = Application.Current.Windows[0] as LoginWindow;
        private MainWindow mainWindow = Application.Current.Windows[0] as MainWindow;

        public EditProfileWindow()
        {
            InitializeComponent();
            DegreeComboBox.Visibility = DegreeLabel.Visibility = Visibility.Hidden;
            PeselTextBox.IsEnabled = PeselLabel.IsEnabled = false;

            GetDataToTextBoxes();
        }

        public void Reset()
        {
            NameTextBox.Text = ""; LastNameTextBox.Text = ""; MiddleNameTextBox.Text = "";
            PeselTextBox.Text = ""; EmailTextBox.Text = ""; PhoneTextBox.Text = "";
            NrDocTextBox.Text = ""; FirstPasswordBox.Password = ""; SecondPasswordBox.Password = "";
            StreetTextBox.Text = ""; NrHouseTextBox.Text = ""; NrFlatTextBox.Text = "";
            PostalCodeTextBox.Text = ""; CityTextBox.Text = ""; ProvidenceTextBox.Text = "";
            CountryTextBox.Text = "";
            DegreeComboBox.Text = "";
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            if (NameTextBox.Text.Length == 0)
            {
                ErrorTextBlock.Text = "Wprowadź imię!";
                NameTextBox.Focus();
            }
            else if (LastNameTextBox.Text.Length == 0)
            {
                ErrorTextBlock.Text = "Wprowadź nazwisko!";
                LastNameTextBox.Focus();
            }
            else if (FirstPasswordBox.Password.Length == 0)
            {
                ErrorTextBlock.Text = "Wprowadź hasło!";
                FirstPasswordBox.Focus();
            }
            else if (SecondPasswordBox.Password.Length == 0)
            {
                ErrorTextBlock.Text = "Wprowadź potwierdzenie hasła!";
                SecondPasswordBox.Focus();
            }
            else if (FirstPasswordBox.Password != SecondPasswordBox.Password)
            {
                ErrorTextBlock.Text = "Oba hasła muszą być takie same!";
                SecondPasswordBox.Focus();
            }
            else if (DegreeComboBox.IsVisible == true && DegreeComboBox.SelectedIndex == -1)
            {
                ErrorTextBlock.Text = "Zaznacz tytuł!";
                DegreeComboBox.Focus();
            }
            else
            {
                ErrorTextBlock.Text = "";
                SaveData();
                MessageBox.Show("Profil zaktualizowany!");
                this.Close();
            }
        }

        private void SaveData()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var student = context.Students
                    .Where(x => x.PESEL == mainWindow.PeselTempLabel.Content.ToString()).FirstOrDefault();
                var guardian = context.Guardians
                    .Where(x => x.PESEL == mainWindow.PeselTempLabel.Content.ToString()).FirstOrDefault();
                var teacher = context.Teachers
                    .Where(x => x.PESEL == mainWindow.PeselTempLabel.Content.ToString()).FirstOrDefault();

                if (student is Students)
                {
                    //MessageBox.Show("Uczeń");

                    student.FirstName = NameTextBox.Text;
                    student.LastName = LastNameTextBox.Text;
                    student.MiddleName = MiddleNameTextBox.Text;
                    student.BirthDate = BirthDatePicker.SelectedDate;
                    student.SchoolIdentityCard = NrDocTextBox.Text;
                    student.PhoneNumber = PhoneTextBox.Text;
                    student.Email = EmailTextBox.Text;
                    student.LoginPassword.Password = FirstPasswordBox.Password;
                    student.LoginPassword.Password = SecondPasswordBox.Password;
                    student.Address.Street = StreetTextBox.Text;
                    student.Address.HouseNumber = NrHouseTextBox.Text;
                    student.Address.FlatNumber = NrFlatTextBox.Text;
                    student.Address.PostalCode = PostalCodeTextBox.Text;
                    student.Address.Place = CityTextBox.Text;
                    student.Address.Province = ProvidenceTextBox.Text;
                    student.Address.Country = CountryTextBox.Text;
                }
                else if (guardian is Guardians)
                {
                    //MessageBox.Show("Opiekun");

                    guardian.FirstName = NameTextBox.Text;
                    guardian.LastName = LastNameTextBox.Text;
                    guardian.MiddleName = MiddleNameTextBox.Text;
                    guardian.BirthDate = BirthDatePicker.SelectedDate;
                    guardian.IdentityCardNumber = NrDocTextBox.Text;
                    guardian.PhoneNumber = PhoneTextBox.Text;
                    guardian.Email = EmailTextBox.Text;
                    guardian.LoginPassword.Password = FirstPasswordBox.Password;
                    guardian.LoginPassword.Password = SecondPasswordBox.Password;
                    guardian.Address.Street = StreetTextBox.Text;
                    guardian.Address.HouseNumber = NrHouseTextBox.Text;
                    guardian.Address.FlatNumber = NrFlatTextBox.Text;
                    guardian.Address.PostalCode = PostalCodeTextBox.Text;
                    guardian.Address.Place = CityTextBox.Text;
                    guardian.Address.Province = ProvidenceTextBox.Text;
                    guardian.Address.Country = CountryTextBox.Text;
                }
                else if (teacher is Teachers)
                {
                    //MessageBox.Show("nauczyciel");

                    teacher.FirstName = NameTextBox.Text;
                    teacher.LastName = LastNameTextBox.Text;
                    teacher.MiddleName = MiddleNameTextBox.Text;
                    teacher.BirthDate = BirthDatePicker.SelectedDate;
                    teacher.IdentityCardNumber = NrDocTextBox.Text;
                    teacher.PhoneNumber = PhoneTextBox.Text;
                    teacher.Email = EmailTextBox.Text;
                    teacher.LoginPassword.Password = FirstPasswordBox.Password;
                    teacher.LoginPassword.Password = SecondPasswordBox.Password;
                    teacher.Degree = (Degree)Enum.Parse(typeof(Degree), DegreeComboBox.Text);
                    teacher.Address.Street = StreetTextBox.Text;
                    teacher.Address.HouseNumber = NrHouseTextBox.Text;
                    teacher.Address.FlatNumber = NrFlatTextBox.Text;
                    teacher.Address.PostalCode = PostalCodeTextBox.Text;
                    teacher.Address.Place = CityTextBox.Text;
                    teacher.Address.Province = ProvidenceTextBox.Text;
                    teacher.Address.Country = CountryTextBox.Text;
                }
                context.SaveChanges();
            }
        }
        private void GetDataToTextBoxes()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var student = context.Students
                    .Where(x => x.PESEL == mainWindow.PeselTempLabel.Content.ToString()).FirstOrDefault();
                var guardian = context.Guardians
                    .Where(x => x.PESEL == mainWindow.PeselTempLabel.Content.ToString()).FirstOrDefault();
                var teacher = context.Teachers
                    .Where(x => x.PESEL == mainWindow.PeselTempLabel.Content.ToString()).FirstOrDefault();

                if (student is Students)
                {
                    //MessageBox.Show("Uczeń");

                    NameTextBox.Text = student.FirstName;
                    LastNameTextBox.Text = student.LastName;
                    MiddleNameTextBox.Text = student.MiddleName;
                    PeselTextBox.Text = student.PESEL;
                    BirthDatePicker.SelectedDate = student.BirthDate;
                    NrDocTextBox.Text = student.SchoolIdentityCard;
                    PhoneTextBox.Text = student.PhoneNumber;
                    EmailTextBox.Text = student.Email;
                    FirstPasswordBox.Password = student.LoginPassword.Password;
                    SecondPasswordBox.Password = student.LoginPassword.Password;
                    StreetTextBox.Text = student.Address.Street;
                    NrHouseTextBox.Text = student.Address.HouseNumber;
                    NrFlatTextBox.Text = student.Address.FlatNumber;
                    PostalCodeTextBox.Text = student.Address.PostalCode;
                    CityTextBox.Text = student.Address.Place;
                    ProvidenceTextBox.Text = student.Address.Province;
                    CountryTextBox.Text = student.Address.Country;
                }
                else if (guardian is Guardians)
                {
                    NameTextBox.Text = guardian.FirstName;
                    LastNameTextBox.Text = guardian.LastName;
                    MiddleNameTextBox.Text = guardian.MiddleName;
                    PeselTextBox.Text = guardian.PESEL;
                    BirthDatePicker.SelectedDate = guardian.BirthDate;
                    NrDocTextBox.Text = guardian.IdentityCardNumber;
                    PhoneTextBox.Text = guardian.PhoneNumber;
                    EmailTextBox.Text = guardian.Email;
                    FirstPasswordBox.Password = guardian.LoginPassword.Password;
                    SecondPasswordBox.Password = guardian.LoginPassword.Password;
                    StreetTextBox.Text = guardian.Address.Street;
                    NrHouseTextBox.Text = guardian.Address.HouseNumber;
                    NrFlatTextBox.Text = guardian.Address.FlatNumber;
                    PostalCodeTextBox.Text = guardian.Address.PostalCode;
                    CityTextBox.Text = guardian.Address.Place;
                    ProvidenceTextBox.Text = guardian.Address.Province;
                    CountryTextBox.Text = guardian.Address.Country;
                }
                else if (teacher is Teachers)
                {
                    DegreeComboBox.Visibility = DegreeLabel.Visibility = Visibility.Visible;

                    //MessageBox.Show("nauczyciel");
                    NameTextBox.Text = teacher.FirstName;
                    LastNameTextBox.Text = teacher.LastName;
                    MiddleNameTextBox.Text = teacher.MiddleName;
                    PeselTextBox.Text = teacher.PESEL;
                    BirthDatePicker.SelectedDate = teacher.BirthDate;
                    NrDocTextBox.Text = teacher.IdentityCardNumber;
                    PhoneTextBox.Text = teacher.PhoneNumber;
                    EmailTextBox.Text = teacher.Email;
                    FirstPasswordBox.Password = teacher.LoginPassword.Password;
                    SecondPasswordBox.Password = teacher.LoginPassword.Password;
                    DegreeComboBox.Text = teacher.Degree.ToString();
                    StreetTextBox.Text = teacher.Address.Street;
                    NrHouseTextBox.Text = teacher.Address.HouseNumber;
                    NrFlatTextBox.Text = teacher.Address.FlatNumber;
                    PostalCodeTextBox.Text = teacher.Address.PostalCode;
                    CityTextBox.Text = teacher.Address.Place;
                    ProvidenceTextBox.Text = teacher.Address.Province;
                    CountryTextBox.Text = teacher.Address.Country;
                }
            }
        }
    }
}
