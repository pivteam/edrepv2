﻿using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolDiary1.Design
{
    /// <summary>
    /// Interaction logic for ClassWindows.xaml
    /// </summary>
    public partial class ClassWindows : Window
    {
        bool buttonAddWasClicked = false;
        bool buttonDeleteWasClicked = false;
        bool buttonEditWasClicked = false;

        public ClassWindows()
        {
            InitializeComponent();
            searchStackPanel.Visibility = Visibility.Hidden;
            saveButton.IsEnabled = clearButton.IsEnabled = StackPanel2.IsEnabled = false;

            personComboBox2.IsEnabled = classComboBox.IsEnabled = false;
            AssignButton.IsEnabled = NoAssignButton.IsEnabled = false;
        }

        private void Enable()
        {
            saveButton.IsEnabled = clearButton.IsEnabled = StackPanel2.IsEnabled = true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            buttonAddWasClicked = true;
            Enable();
            stackPanel1.IsEnabled = false;
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            buttonDeleteWasClicked = true;
            Enable();
            stackPanel1.IsEnabled = false;
        }
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            buttonEditWasClicked = true;
            searchStackPanel.Visibility = Visibility.Visible;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            if (!Regex.IsMatch(classTextBox.Text, "[0-9][a-z]"))
            {
                MessageBox.Show("Nieprawidłowa nazwa klasy!");
                classTextBox.Focus();
            }
            else
            {
                if (buttonAddWasClicked == true)
                    AddClass();
                else if (buttonDeleteWasClicked == true)
                    DeleteClass();
                else if (buttonEditWasClicked == true)
                    EditClass();
            }
        }

        private void AddClass()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                Classes clas = new Classes()
                {
                    ClassName = classTextBox.Text
                };

                var klas = context.Classes.Where(x => x.ClassName == classTextBox.Text).FirstOrDefault();

                if (klas != null)
                {
                    MessageBox.Show("Isnieje już klasa o takiej nazwie!");
                    classTextBox.Focus();
                }
                else
                {
                    context.Classes.Add(clas);
                    context.SaveChanges();

                    MessageBox.Show("Klasa dodana!");

                    buttonAddWasClicked = false;
                    stackPanel1.IsEnabled = true;
                    StackPanel2.IsEnabled = false;
                    classTextBox.Text = null;
                    searchTextBox.Text = null;
                    saveButton.IsEnabled = false;
                    clearButton.IsEnabled = false;
                }
            }
        }
        private void DeleteClass()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var klasToDelete = context.Classes.Where(x => x.ClassName == classTextBox.Text).FirstOrDefault();

                if (klasToDelete != null)
                {
                    context.Classes.Remove(klasToDelete);
                    context.SaveChanges();

                    MessageBox.Show("Klasa usunięta!");

                    buttonDeleteWasClicked = false;
                    classTextBox.Text = null;
                    stackPanel1.IsEnabled = true;
                    StackPanel2.IsEnabled = false;
                    saveButton.IsEnabled = false;
                    clearButton.IsEnabled = false;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono klasy o takiej nazwie!");
                    classTextBox.Focus();
                }
            }
        }
        private void EditClass()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var classToEdit = context.Classes.Where(x => x.ClassName == searchTextBox.Text).SingleOrDefault();

                classToEdit.ClassName = classTextBox.Text;

                context.SaveChanges();

                buttonEditWasClicked = false;
                classTextBox.Text = null;
                stackPanel1.IsEnabled = true;
                saveButton.IsEnabled = false;
                clearButton.IsEnabled = false;
                StackPanel2.IsEnabled = false;

                MessageBox.Show("Zaktualizowano!");
            }

        }

        private void searchTextBox_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            searchTextBox.Clear();
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var classToEdit = context.Classes.Where(x => x.ClassName == searchTextBox.Text).SingleOrDefault();

                if (classToEdit != null)
                {
                    searchStackPanel.Visibility = Visibility.Hidden;
                    StackPanel2.IsEnabled = true;

                    saveButton.IsEnabled = true;
                    clearButton.IsEnabled = true;

                    classTextBox.Text = classToEdit.ClassName;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono takiej klasy!");
                    searchTextBox.Focus();
                }
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            classTextBox.Text = searchTextBox.Text = null;
            stackPanel1.IsEnabled = true;
            saveButton.IsEnabled = clearButton.IsEnabled = false;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void radioButton_Click(object sender, RoutedEventArgs e)
        {
            personComboBox2.SelectedIndex = -1;
            classComboBox.SelectedIndex = -1;

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                List<string> teacherList = new List<string>();
                var teacher = context.Teachers.Where(x => x.PESEL != null).ToList();

                foreach (var item in teacher)
                    teacherList.Add(item.PESEL);

                personComboBox2.ItemsSource = teacherList;
            }
            personComboBox2.IsEnabled = true;

            AddClassToComboBox();
        }

        private void radioButton2_Click(object sender, RoutedEventArgs e)
        {
            personComboBox2.SelectedIndex = -1;
            classComboBox.SelectedIndex = -1;

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                List<string> studentList = new List<string>();
                var student = context.Students.Where(x => x.PESEL != null).ToList();

                foreach (var item in student)
                    studentList.Add(item.PESEL);

                personComboBox2.ItemsSource = studentList;
            }
            personComboBox2.IsEnabled = true;

            AddClassToComboBox();
        }
        private void AddClassToComboBox()
        {
            classComboBox.Items.Refresh();
            List<string> itemList = new List<string>();

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var classes = context.Classes
                    .Where(x => x.ClassName != null).ToList()
                    .OrderBy(x => x.ClassName);

                foreach (var item in classes)
                    itemList.Add(item.ClassName);

                classComboBox.ItemsSource = itemList;
            }
        }

        private void ClearButton_Click_1(object sender, RoutedEventArgs e)
        {
            TeacherRadioButton.IsChecked = StudentRadioButton.IsChecked = false;
            personComboBox2.SelectedIndex = classComboBox.SelectedIndex = -1;
            personComboBox2.IsEnabled = classComboBox.IsEnabled = false;
        }

        private void personComboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            classComboBox.IsEnabled = true;
        }

        private void classComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AssignButton.IsEnabled = NoAssignButton.IsEnabled = true;
        }

        private void AssignButton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var clas = context.Classes.Where(x => x.ClassName == classComboBox.SelectedItem.ToString()).FirstOrDefault();

                if (StudentRadioButton.IsChecked == true)
                {
                    var student = context.Students.Where(x => x.PESEL == personComboBox2.SelectedItem.ToString()).FirstOrDefault();

                    if (clas.Students.Contains(student))
                        MessageBox.Show("Uczeń jest już przypisany do tek klasy!");
                    else
                    {
                        clas.Students.Add(student);
                        MessageBox.Show("Uczeń przypisany do klasy!");
                    }
                }
                else if (TeacherRadioButton.IsChecked == true)
                {
                    var teacher = context.Teachers.Single(x => x.PESEL == personComboBox2.SelectedItem.ToString());


                    if (teacher.Classes.Contains(clas))
                        MessageBox.Show("Nauczyciel jest już przypisany do tej klasy!");
                    else
                    {
                        teacher.Classes.Add(clas);
                        MessageBox.Show("Nauczyciel przypisany do klasy!");
                    }
                }
                context.SaveChanges();
            }
        }

        private void NoAssignButton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var clas = context.Classes.Where(x => x.ClassName == classComboBox.SelectedItem.ToString()).FirstOrDefault();

                if (StudentRadioButton.IsChecked == true)
                {
                    var student = context.Students.Where(x => x.PESEL == personComboBox2.SelectedItem.ToString()).FirstOrDefault();      

                    if (clas.Students.Contains(student))
                    {
                        clas.Students.Remove(student);
                        MessageBox.Show("Uczeń wypisany!");
                    }
                    else
                        MessageBox.Show("Uczeń nie jest przypisany to dej klasy!");

                    context.SaveChanges();
                }
                else if (TeacherRadioButton.IsChecked == true)
                {
                    var teacher = context.Teachers.Single(x => x.PESEL == personComboBox2.SelectedItem.ToString());

                    if (teacher.Classes.Contains(clas))
                    {
                        teacher.Classes.Remove(clas);
                        MessageBox.Show("Nauczyciel wypisany!");
                    }
                    else
                        MessageBox.Show("Nauczyciel nie jest przypisany to dej klasy!");

                    context.SaveChanges();
                }
            }
        }
    }
}
