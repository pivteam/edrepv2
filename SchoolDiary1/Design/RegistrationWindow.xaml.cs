﻿using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolDiary1.Design
{
    /// <summary>
    /// Interaction logic for RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        LoginWindow logWindow;
        public RegistrationWindow()
        {
            InitializeComponent();

            DegreeLabel.Visibility = Visibility.Visible;
            DegreeComboBox.Visibility = Visibility.Visible;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }



        public void Reset()
        {
            NameTextBox.Text = ""; LastNameTextBox.Text = ""; MiddleNameTextBox.Text = "";
            PeselTextBox.Text = ""; EmailTextBox.Text = ""; PhoneTextBox.Text = "";
            NrDocTextBox.Text = ""; FirstPasswordBox.Password = ""; SecondPasswordBox.Password = "";
            StreetTextBox.Text = ""; NrHouseTextBox.Text = ""; NrFlatTextBox.Text = "";
            PostalCodeTextBox.Text = ""; CityTextBox.Text = ""; ProvidenceTextBox.Text = "";
            CountryTextBox.Text = "";
            DegreeComboBox.Text = "";
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            logWindow = new LoginWindow();
            logWindow.Show();
            this.Close();
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            if (PeselTextBox.Text.Length == 0)
            {
                ErrorTextBlock.Text = "Wprowadź pesel!";
                PeselTextBox.Focus();
            }
            else if (!Regex.IsMatch(PeselTextBox.Text, "^[0-9]+$"))
            {
                ErrorTextBlock.Text = "Pesel może zawierać tylko cyfry!";
                PeselTextBox.Focus();
            }
            else if (PeselTextBox.Text.Length != 11)
            {
                ErrorTextBlock.Text = "Pesel musi zawierać 11 cyfr!";
                PeselTextBox.Focus();
            }
            else
            {
                if (NameTextBox.Text.Length == 0)
                {
                    ErrorTextBlock.Text = "Wprowadź imię!";
                    NameTextBox.Focus();
                }
                else if (LastNameTextBox.Text.Length == 0)
                {
                    ErrorTextBlock.Text = "Wprowadź nazwisko!";
                    LastNameTextBox.Focus();
                }
                else if (FirstPasswordBox.Password.Length == 0)
                {
                    ErrorTextBlock.Text = "Wprowadź hasło!";
                    FirstPasswordBox.Focus();
                }
                else if (SecondPasswordBox.Password.Length == 0)
                {
                    ErrorTextBlock.Text = "Wprowadź potwierdzenie hasła!";
                    SecondPasswordBox.Focus();
                }
                else if (FirstPasswordBox.Password != SecondPasswordBox.Password)
                {
                    ErrorTextBlock.Text = "Oba hasła muszą być takie same!";
                    SecondPasswordBox.Focus();
                }
                else if (TeacherRadioButton.IsChecked == true && DegreeComboBox.SelectedIndex == -1)
                {
                    ErrorTextBlock.Text = "Zaznacz tytuł!";
                    DegreeComboBox.Focus();
                }
                else
                {
                    ErrorTextBlock.Text = "";
                    if (TeacherRadioButton.IsChecked == true)
                        AddTeacher();
                    if (GuardianRadioButton.IsChecked == true)
                        AddGuardian();
                    if (StudentRadioButton.IsChecked == true)
                        AddStudent();


                }
            }
        }

        private void AddTeacher()
        {
            using (SchoolDiaryContext db = new SchoolDiaryContext())
            {
                var teach = db.Teachers
                    .Where(x => x.PESEL == PeselTextBox.Text).FirstOrDefault();

                var logPass = db.LoginsPasswords.Where(x => x.Login == PeselTextBox.Text).FirstOrDefault();

                if (logPass == null || teach == null)
                {
                    MessageBox.Show("Nie ma utworzonego konta Nauczyciele o takim peselu!\nSkontaktuj się z adminem.");
                    PeselTextBox.Focus();
                }
                else if (logPass.Password != null)
                {
                    MessageBox.Show("Osoba o takim peselu jest już zarejestrowana!");
                }
                else
                {
                    teach.FirstName = NameTextBox.Text;
                    teach.LastName = LastNameTextBox.Text;
                    teach.MiddleName = MiddleNameTextBox.Text;
                    teach.PESEL = PeselTextBox.Text;
                    teach.BirthDate = BirthDatePicker.SelectedDate;
                    teach.IdentityCardNumber = NrDocTextBox.Text;
                    teach.PhoneNumber = PhoneTextBox.Text;
                    teach.Email = EmailTextBox.Text;
                    teach.Degree = (Degree)Enum.Parse(typeof(Degree), DegreeComboBox.Text);

                    Addresses adres = new Addresses()
                    {
                        Street = StreetTextBox.Text,
                        HouseNumber = NrHouseTextBox.Text,
                        FlatNumber = NrFlatTextBox.Text,
                        PostalCode = PostalCodeTextBox.Text,
                        Place = CityTextBox.Text,
                        Province = ProvidenceTextBox.Text,
                        Country = CountryTextBox.Text,
                    };

                    db.Addresses.Add(adres);
                    adres.Teachers.Add(teach);     //one to many

                    logPass.Login = PeselTextBox.Text;
                    logPass.Password = FirstPasswordBox.Password;

                    teach.LoginPassword = logPass;          // relacja one to one

                    db.SaveChanges();

                    MessageBox.Show("Rejestracja udana");
                    Reset();
                    logWindow = new LoginWindow();
                    logWindow.Show();
                    this.Close();
                }
            }
        }
        private void AddGuardian()
        {
            using (SchoolDiaryContext db = new SchoolDiaryContext())
            {
                var guard = db.Guardians.Where(x => x.PESEL == PeselTextBox.Text).FirstOrDefault();
                var logPass = db.LoginsPasswords.Where(x => x.Login == PeselTextBox.Text).FirstOrDefault();


                if (logPass == null || guard == null)
                {
                    MessageBox.Show("Nie ma utworzonego konta Opiekunowie o takim peselu!\nSkontaktuj się z adminem.");
                    PeselTextBox.Focus();
                }
                else if (logPass.Password != null)
                {
                    MessageBox.Show("Osoba o takim peselu jest już zarejestrowana!");
                }
                else
                {
                    guard.FirstName = NameTextBox.Text;
                    guard.LastName = LastNameTextBox.Text;
                    guard.MiddleName = MiddleNameTextBox.Text;
                    guard.PESEL = PeselTextBox.Text;
                    guard.BirthDate = BirthDatePicker.SelectedDate;
                    guard.IdentityCardNumber = NrDocTextBox.Text;
                    guard.PhoneNumber = PhoneTextBox.Text;
                    guard.Email = EmailTextBox.Text;

                    Addresses adres = new Addresses()
                    {
                        Street = StreetTextBox.Text,
                        HouseNumber = NrHouseTextBox.Text,
                        FlatNumber = NrFlatTextBox.Text,
                        PostalCode = PostalCodeTextBox.Text,
                        Place = CityTextBox.Text,
                        Province = ProvidenceTextBox.Text,
                        Country = CountryTextBox.Text
                    };

                    db.Addresses.Add(adres);
                    adres.Guardians.Add(guard);     //one to many

                    logPass.Login = PeselTextBox.Text;
                    logPass.Password = FirstPasswordBox.Password;

                    guard.LoginPassword = logPass;          // relacja one to one

                    db.SaveChanges();

                    MessageBox.Show("Rejestracja udana");
                    Reset();
                    logWindow = new LoginWindow();
                    logWindow.Show();
                    this.Close();
                }
            }
        }
        private void AddStudent()
        {
            using (SchoolDiaryContext db = new SchoolDiaryContext())
            {
                var stud = db.Students.Where(x => x.PESEL == PeselTextBox.Text).FirstOrDefault();
                var logPass = db.LoginsPasswords.Where(x => x.Login == PeselTextBox.Text).FirstOrDefault();

                if (logPass == null || stud == null)
                {
                    MessageBox.Show("Nie ma utworzonego konta Studenci o takim peselu!\nSkontaktuj się z adminem.");
                    PeselTextBox.Focus();
                }
                else if (logPass.Password != null)
                {
                    MessageBox.Show("Osoba o takim peselu jest już zarejestrowana!");
                }
                else
                {
                    stud.FirstName = NameTextBox.Text;
                    stud.LastName = LastNameTextBox.Text;
                    stud.MiddleName = MiddleNameTextBox.Text;
                    stud.PESEL = PeselTextBox.Text;
                    stud.BirthDate = BirthDatePicker.SelectedDate;
                    stud.SchoolIdentityCard = NrDocTextBox.Text;
                    stud.PhoneNumber = PhoneTextBox.Text;
                    stud.Email = EmailTextBox.Text;
                    Addresses adres = new Addresses()
                    {
                        Street = StreetTextBox.Text,
                        HouseNumber = NrHouseTextBox.Text,
                        FlatNumber = NrFlatTextBox.Text,
                        PostalCode = PostalCodeTextBox.Text,
                        Place = CityTextBox.Text,
                        Province = ProvidenceTextBox.Text,
                        Country = CountryTextBox.Text
                    };


                    db.Addresses.Add(adres);
                    adres.Students.Add(stud);     //one to many

                    logPass.Login = PeselTextBox.Text;
                    logPass.Password = FirstPasswordBox.Password;

                    stud.LoginPassword = logPass;          // relacja one to one

                    db.SaveChanges();

                    MessageBox.Show("Rejestracja udana");
                    Reset();
                    logWindow = new LoginWindow();
                    logWindow.Show();
                    this.Close();
                }
            }
        }

        private void GuardianRadioButton_Click(object sender, RoutedEventArgs e)
        {
            DegreeLabel.Visibility = Visibility.Hidden;
            DegreeComboBox.Visibility = Visibility.Hidden;
        }

        private void StudentRadioButton_Click(object sender, RoutedEventArgs e)
        {
            DegreeLabel.Visibility = Visibility.Hidden;
            DegreeComboBox.Visibility = Visibility.Hidden;
        }

        private void TeacherRadioButton_Click(object sender, RoutedEventArgs e)
        {
            DegreeLabel.Visibility = Visibility.Visible;
            DegreeComboBox.Visibility = Visibility.Visible;
        }
    }
}
