﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SchoolDiary1.Model;

namespace SchoolDiary1.Design
{
    /// <summary>
    /// Interaction logic for RateWindow.xaml
    /// </summary>
    public partial class RateWindow : Window
    {
        Dictionary<string, string> studDic = new Dictionary<string, string>();
        private MainWindow mainWindow = Application.Current.Windows[0] as MainWindow;
        public RateWindow()
        {
            InitializeComponent();
            List<string> classesList = new List<string>();
            
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var classes = context.Classes
                    .Where(x => x.ClassName != null && (x.ClassesSubjects.Any(y=> y.Teacher.PESEL== mainWindow.PeselTempLabel.Content.ToString())) || mainWindow.NameTextBox.Text=="admin").ToList()
                    .OrderBy(x => x.ClassName);
                
                foreach (var item in classes)
                    classesList.Add(item.ClassName);

                ClassesBox.ItemsSource = classesList;
            }

            //SubjectsBox.Items.Refresh();

        }

        private void Show_Click(object sender, RoutedEventArgs e)
        {
            studDic.Clear();
            StudentsList.ItemsSource = null;
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var Class = context.Classes.Where(x => x.ClassName == ClassesBox.SelectedItem.ToString()).FirstOrDefault();
                var Subject = context.Subjects.Where(x => x.SubjectName == SubjectsBox.SelectedItem.ToString()).FirstOrDefault();
                var student = context.Students.Where(x => x.PESEL != null).ToList();

                foreach (var item in student)
                {
                    if (item.Class.ClassesId == Class.ClassesId)
                        studDic.Add(item.PESEL, item.LastName + " " + item.FirstName);
                }

                StudentsList.ItemsSource = studDic.Values;

                //Edit.IsEnabled = true;

                var students = (from s in context.Students
                                join c in context.CardSubjects on s.StudentsId equals c.Student.StudentsId
                                where c.Subject.SubjectName == Subject.SubjectName && s.Class.ClassName == Class.ClassName
                                select new
                                {
                                    Nazwisko = s.LastName,
                                    Imię = s.FirstName,
                                    Koń = c.FinalGrade
                                }).OrderBy(x => x.Nazwisko);

                FinalRateGrid.ItemsSource = students.ToList();



            }
        }

        private void ClassesBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SubjectsBox.SelectedIndex < 0)
                Show.IsEnabled = false;
            if (ClassesBox.SelectedIndex != -1)
            {
                List<string> subjectsList = new List<string>();

                using (SchoolDiaryContext context = new SchoolDiaryContext())
                {
                    var Class = context.Classes.Where(x => x.ClassName == ClassesBox.SelectedItem.ToString()).FirstOrDefault();
                    var subjects = context.ClassesSubjects
                        .Where(x => (x.Class.ClassesId == Class.ClassesId && x.Teacher.PESEL==mainWindow.PeselTempLabel.Content.ToString())||(x.Class.ClassesId == Class.ClassesId && mainWindow.NameTextBox.Text=="admin")).ToList();

                    foreach (var item in subjects)
                    {

                        subjectsList.Add(item.Subject.SubjectName);
                    }

                    SubjectsBox.ItemsSource = subjectsList;
                }

                SubjectsBox.IsEnabled = true;
            }
        }

        private void SubjectsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SubjectsBox.SelectedIndex != -1)
                Show.IsEnabled = true;
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            editPanel.IsEnabled = true;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void StudentsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            saveButton.IsEnabled = true;
            CancleSaveButton.IsEnabled = true;
            StudentsList.IsEnabled = false;
            FinalRateBox.IsEnabled = true;
            editPanel.IsEnabled = true;
            Show.IsEnabled = false;
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var Subject = context.Subjects.Where(x => x.SubjectName == SubjectsBox.SelectedItem.ToString()).FirstOrDefault();
                string tmp = studDic.FirstOrDefault(x => x.Value.Contains(StudentsList.SelectedItem.ToString())).Key;
                var student = context.Students.Where(x => x.PESEL == tmp).FirstOrDefault();
                var cardSub = context.CardSubjects.Where(x => x.Student.StudentsId == student.StudentsId && x.Subject.SubjectName == Subject.SubjectName);
                List<int> NumberList = new List<int>();
                //RateGrid.ItemsSource = cardSub.ToList();
                foreach (var item in cardSub)
                {
                    foreach (var item2 in item.Grades)
                    {
                        NumberList.Add(item2.Number);
                    }
                    RateNumbers.ItemsSource = NumberList;


                }

            }

        }
        void refreshList()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var Subject = context.Subjects.Where(x => x.SubjectName == SubjectsBox.SelectedItem.ToString()).FirstOrDefault();
                string tmp = studDic.FirstOrDefault(x => x.Value.Contains(StudentsList.SelectedItem.ToString())).Key;
                var student = context.Students.Where(x => x.PESEL == tmp).FirstOrDefault();
                var cardSub = context.CardSubjects.Where(x => x.Student.StudentsId == student.StudentsId && x.Subject.SubjectName == Subject.SubjectName);

                //RateGrid.ItemsSource = cardSub.ToList();
                foreach (var item in cardSub)
                {
                    FinalRateBox.Text = item.FinalGrade.ToString();
                    var grade = item.Grades.Select(x => new
                    {
                        Numer = x.Number,
                        Ocena = x.Grade,
                        Waga = x.GradeWeight,
                        Data = x.Date,
                        Opis = x.Contents
                    });
                    RateGrid.ItemsSource = grade.ToList();

                }

            }
        }
        private void StudentsList_MouseUp(object sender, MouseButtonEventArgs e)
        {
            refreshList();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var Subject = context.Subjects.Where(x => x.SubjectName == SubjectsBox.SelectedItem.ToString()).FirstOrDefault();
                string tmp = studDic.FirstOrDefault(x => x.Value.Contains(StudentsList.SelectedItem.ToString())).Key;
                var student = context.Students.Where(x => x.PESEL == tmp).FirstOrDefault();
                var cardSub = context.CardSubjects.Where(x => x.Student.StudentsId == student.StudentsId);

                foreach (var item in cardSub)
                {
                    if (item.Subject.SubjectName == Subject.SubjectName)
                        item.FinalGrade = Convert.ToInt32(FinalRateBox.Text);
                }
                context.SaveChanges();
            }
            saveButton.IsEnabled = false;
            CancleSaveButton.IsEnabled = false;
            StudentsList.IsEnabled = true;
            FinalRateBox.IsEnabled = false;
            Show.IsEnabled = true;
            Show_Click(sender, e);


        }

        private void CancleSaveButton_Click(object sender, RoutedEventArgs e)
        {
            saveButton.IsEnabled = false;
            CancleSaveButton.IsEnabled = false;
            StudentsList.IsEnabled = true;
            FinalRateBox.IsEnabled = false;
            editPanel.IsEnabled = false;
            Show.IsEnabled = true;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            editPanel.IsEnabled = false;
            saveButton.IsEnabled = false;
            CancleSaveButton.IsEnabled = false;
            StudentsList.IsEnabled = true;
            FinalRateBox.IsEnabled = false;
            Show.IsEnabled = true;

            if (REdit.IsChecked == true)
            {
                using (SchoolDiaryContext context = new SchoolDiaryContext())
                {
                    var Subject = context.Subjects.Where(x => x.SubjectName == SubjectsBox.SelectedItem.ToString()).FirstOrDefault();
                    string tmp = studDic.FirstOrDefault(x => x.Value.Contains(StudentsList.SelectedItem.ToString())).Key;
                    var student = context.Students.Where(x => x.PESEL == tmp).FirstOrDefault();
                    var cardSub = context.CardSubjects.Where(x => x.Student.StudentsId == student.StudentsId && x.Subject.SubjectName == Subject.SubjectName);

                    foreach (var item in cardSub)
                    {
                        var tmpGrade = item.Grades.Where(x => x.Number.ToString() == RateNumbers.SelectedItem.ToString());
                        foreach (var item2 in tmpGrade)
                        {
                            item2.Grade = Convert.ToInt32(RateBox.Text);
                            item2.GradeWeight = Convert.ToInt32(WeightBox.Text);
                            item2.Contents = ContentstBox.Text;
                            item2.Date = (DateTime)DatePicker.SelectedDate;
                        }

                    }
                    context.SaveChanges();
                }
            }
            else if (RAdd.IsChecked == true)
            {
                if (RateNumbers.Items.Contains(RateNumberBox.Text)==true)
                {
                    MessageBox.Show("Prosze wybrać inny numer oceny lub zmienić na edycje");
                }
                else if (RateNumbers.Items.Contains(RateNumberBox.Text)==false)
                {
                    using (SchoolDiaryContext context = new SchoolDiaryContext())
                    {
                        var Subject = context.Subjects.Where(x => x.SubjectName == SubjectsBox.SelectedItem.ToString()).FirstOrDefault();
                        string tmp = studDic.FirstOrDefault(x => x.Value.Contains(StudentsList.SelectedItem.ToString())).Key;
                        var student = context.Students.Where(x => x.PESEL == tmp).FirstOrDefault();
                        var cardSub = context.CardSubjects.Where(x => x.Student.StudentsId == student.StudentsId && x.Subject.SubjectName == Subject.SubjectName);

                        foreach (var item in cardSub)
                        {
                            var newGrade = new Grades()
                            {
                                Number = Convert.ToInt32(RateNumberBox.Text),
                                Grade = Convert.ToInt32(RateBox.Text),
                                GradeWeight = Convert.ToInt32(WeightBox.Text),
                                Contents = ContentstBox.Text,
                                Date = (DateTime)DatePicker.SelectedDate
                            };
                            RateNumberBox.Clear();
                            context.Grades.Add(newGrade);
                            item.Grades.Add(newGrade);
                        }
                        context.SaveChanges();
                    }
                }
            }
            refreshList();
        }

        private void EditCancle_Click(object sender, RoutedEventArgs e)
        {
            editPanel.IsEnabled = false;
            saveButton.IsEnabled = false;
            CancleSaveButton.IsEnabled = false;
            StudentsList.IsEnabled = true;
            FinalRateBox.IsEnabled = false;
            Show.IsEnabled = true;
        }

        private void REdit_Checked(object sender, RoutedEventArgs e)
        {
            RateNumbers.IsEnabled = true;
            RateNumberBox.IsEnabled = false;

        }

        private void RAdd_Checked(object sender, RoutedEventArgs e)
        {
            RateNumbers.IsEnabled = false;
            RateNumberBox.IsEnabled = true;
        }

        private void RateNumbers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var Subject = context.Subjects.Where(x => x.SubjectName == SubjectsBox.SelectedItem.ToString()).FirstOrDefault();
                string tmp = studDic.FirstOrDefault(x => x.Value.Contains(StudentsList.SelectedItem.ToString())).Key;
                var student = context.Students.Where(x => x.PESEL == tmp).FirstOrDefault();
                var cardSub = context.CardSubjects.Where(x => x.Student.StudentsId == student.StudentsId && x.Subject.SubjectName == Subject.SubjectName);

                foreach (var item in cardSub)
                {
                    var tmpGrade = item.Grades.Where(x => x.Number.ToString() == RateNumbers.SelectedItem.ToString());
                    foreach (var item2 in tmpGrade)
                    {
                        RateBox.Text = item2.Grade.ToString();
                        WeightBox.Text = item2.GradeWeight.ToString();
                        ContentstBox.Text = item2.Contents.ToString();
                        DatePicker.SelectedDate = item2.Date;
                    }

                }

            }
        }
    }
}
