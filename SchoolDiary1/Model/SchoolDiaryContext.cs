namespace SchoolDiary1.Model
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class SchoolDiaryContext : DbContext
    {
        public SchoolDiaryContext()
            : base("name=SchoolDiaryContext")
        {
        }

        public DbSet<Addresses> Addresses { get; set; }
        public DbSet<Attendances> Attendances { get; set; }
        public DbSet<CardSubjects> CardSubjects { get; set; }
        public DbSet<Classes> Classes { get; set; }
        public DbSet<ClassesSubjects> ClassesSubjects { get; set; }
        public DbSet<Grades> Grades { get; set; }
        public DbSet<Guardians> Guardians { get; set; }
        public DbSet<NewsLetter> NewsLetter { get; set; }
        public DbSet<Notes> Notes { get; set; }
        public DbSet<Students> Students { get; set; }
        public DbSet<Subjects> Subjects { get; set; }
        public DbSet<Teachers> Teachers { get; set; }
        public DbSet<LoginsPasswords> LoginsPasswords { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
