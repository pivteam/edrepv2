﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public enum AttendanceType
    {
        present, //obecny
        absent, //nieobecny
        belated //spóźniony
    };

    public class Attendances //obecności
    {
        public Attendances()
        {

        }
        [Key]
        public int AttendancesId { get; set; }
        public AttendanceType AttendanceType { get; set; }
        public DateTime Date { get; set; }
        public string Contents { get; set; }

        //public int ClassesSubjectsId { get; set; }
        //public int StudentsId { get; set; }

        //[ForeignKey("StudentsId")]
        public virtual Students Student { get; set; }

        //[ForeignKey("ClassesSubjectsId")]
        public virtual ClassesSubjects ClassSubject { get; set; }
    }
}
