﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public class ClassesSubjects
    {
        public ClassesSubjects()
        {
            Attendances = new HashSet<Attendances>();
        }
        [Key]
        public int ClassesSubjectsId { get; set; }

        //public int SubjectsId { get; set; }
        //public int ClassesId { get; set; }
        //public int TeachersId { get; set; }

        public virtual ICollection<Attendances> Attendances { get; set; }

        //[ForeignKey("SubjectsId")]
        public virtual Subjects Subject { get; set; }

       // [ForeignKey("TeachersId")]
        public virtual Teachers Teacher { get; set; }

       // [ForeignKey("ClassesId")]
        public virtual Classes Class { get; set; }
    }
}
