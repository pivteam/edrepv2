﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public class LoginsPasswords
    {
        public LoginsPasswords()
        {

        }

        [Key]
        public int LoginsPasswordsId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public virtual Students Student { get; set; }
        public virtual Teachers Teacher { get; set; }
        public virtual Guardians Guardian { get; set; }
    }
}
