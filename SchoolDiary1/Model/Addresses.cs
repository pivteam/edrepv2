﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public class Addresses
    {
        public Addresses()
        {
            Guardians = new HashSet<Guardians>();
            Students = new HashSet<Students>();
            Teachers = new HashSet<Teachers>();
        }
        [Key]
        public int? AddressesId { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string FlatNumber { get; set; }
        public string PostalCode { get; set; }
        public string Place { get; set; }
        public string Province { get; set; }        //województwo
        public string Country { get; set; }

        public virtual ICollection<Guardians> Guardians { get; set; }
        public virtual ICollection<Students> Students { get; set; }
        public virtual ICollection<Teachers> Teachers { get; set; }

    }
}
