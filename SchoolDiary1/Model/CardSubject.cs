﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public class CardSubjects
    {
        public CardSubjects()
        {
            Grades = new HashSet<Grades>();
        }
        [Key]
        public int CardSubjectsId { get; set; }
        public int FinalGrade { get; set; }

        //public int StudentsId { get; set; }
        //public int TeachersId { get; set; }
        //public int SubjectsId { get; set; }

        public virtual ICollection<Grades> Grades { get; set; }

        //[ForeignKey("TeachersId")]
        public virtual Teachers Teacher { get; set; }

        //[ForeignKey("SubjectsId")]
        public virtual Subjects Subject { get; set; }

        //[ForeignKey("StudentsId")]
        public virtual Students Student { get; set; }
    }
}
