﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public class Classes
    {
        public Classes()
        {
            Students = new HashSet<Students>();
            NewsLetters = new HashSet<NewsLetter>();
            ClassesSubjects = new HashSet<ClassesSubjects>();
        }
        [Key]
        public int ClassesId { get; set; }
        public string ClassName { get; set; }

        //public int TeachersId { get; set; }

        public virtual ICollection<Students> Students { get; set; }

       // [ForeignKey("TeachersId")]
        public virtual Teachers Teacher { get; set; }
        public virtual ICollection<NewsLetter> NewsLetters { get; set; }
        public virtual ICollection<ClassesSubjects> ClassesSubjects { get; set; }
    }
}
