﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    //uwagi
    public class Notes
    {
        public Notes()
        {

        }
        [Key]
        public int NotesId { get; set; }
        public string Contents { get; set; }
        public DateTime Date { get; set; }

        //public int TeachersId { get; set; }
        //public int StudentsId { get; set; }


        //[ForeignKey("StudentsId")]
        public virtual Students Student { get; set; }

       // [ForeignKey("TeachersId")]
        public virtual Teachers Teacher { get; set; }
    }
}
