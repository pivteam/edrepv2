﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{

    public class Grades
    {
        public Grades()
        {
            CardSubjects = new HashSet<CardSubjects>();
        }
        [Key]
        public int GradesId { get; set; }
        public int Grade { get; set; }
        public int Number { get; set; }
        public int GradeWeight { get; set; }
        public DateTime Date { get; set; }
        public string Contents { get; set; }

        public virtual ICollection<CardSubjects> CardSubjects { get; set; }
    }
}
