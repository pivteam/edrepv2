﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public class NewsLetter
    {
        public NewsLetter()
        {

        }
        [Key]
        public int NewsLetterId { get; set; }
        public string Contents { get; set; }
        public DateTime Date { get; set; }

        //public int TeachersId { get; set; }
        //public int ClassesId { get; set; }

        //[ForeignKey("TeachersId")]
        public virtual Teachers Teacher { get; set; }

       // [ForeignKey("ClassesId")]
        public virtual Classes Class { get; set; }
    }
}
