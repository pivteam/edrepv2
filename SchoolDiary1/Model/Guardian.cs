﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolDiary1.Model
{
    public class Guardians
    {
        public Guardians()
        {
            Students = new HashSet<Students>();
        }
        [Key, ForeignKey("LoginPassword")]
        public int GuardiansId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PESEL { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string IdentityCardNumber { get; set; }

        //public int AddressesId { get; set; }

        public virtual ICollection<Students> Students { get; set; }

        //[ForeignKey("AddressesId")]
        public virtual Addresses Address { get; set; }

        //[ForeignKey("LoginsPasswordsId")]
        public virtual LoginsPasswords LoginPassword { get; set; }

    }
}
